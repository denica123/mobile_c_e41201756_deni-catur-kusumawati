import 'package:week2/week2.dart' as week2;

void main(List<String> args) {
  for (int x = 1; x <= 20; x++){
    if (x % 3 == 0 && x % 2 == 1){
      print("$x - Love Coding");
    } else if (x % 2 == 0){
      print("$x - Berkualitas");
    } else if (x % 2 == 1){
      print("$x - Santai");
    }
  }
}