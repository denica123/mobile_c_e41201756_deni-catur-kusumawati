import 'package:week2/week2.dart' as week2;
import 'dart:io';

void main(List<String> args) {
  stdout.write("Masukkan Nama :");
  String nama = stdin.readLineSync()!;
  stdout.write("Pilih peran :");
  String peran = stdin.readLineSync()!;

  if (nama == ''){
    print('Nama harus di isi');
  } else if (peran == ''){
    print('Halo $nama, Pilih peranmu untuk memulai game');
  } else if(peran == 'Penyihir'){
    print('Selamat datang di Dunia Werewolf, $nama.' + ' Hallo $peran $nama, kamu dapat melihat siapa yang  menjadi Werewolf');
  } else if(peran == 'Guard'){
    print('Selamat datang di Dunia Werewolf, $nama.' + ' Hallo $peran $nama, kamu akan membantu melindungi temanmu dari serangan Werewolf');
  } else if( peran == 'Werewolf'){
    print('Selamat datang di Dunia Werewolf, $nama.' + ' Hallo $peran $nama, kamu akan memakan mangsa setiap malam!');
  } else {
    print('Error!');
  }
}