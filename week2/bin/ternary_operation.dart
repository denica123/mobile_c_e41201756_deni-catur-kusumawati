import 'package:week2/week2.dart' as week2;
import 'dart:io';

void main(List<String> arguments) {
  print('Apakah anda akan menginstall aplikasi dart?');
  stdout.write('Y/T ..?');
  String answer = stdin.readLineSync()!;

  var result = (answer == 'y')
    ? print('Anda akan menginstall aplikasi dart')
    : print('Aborted');

  
}
