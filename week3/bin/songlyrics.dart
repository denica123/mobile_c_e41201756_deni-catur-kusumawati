import 'package:week3/week3.dart' as week3;

void main(List<String> args) async {
  print("Ready. Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async{
  String lirik = "Ku kira kita asam dan garam";
  return await Future.delayed(Duration(seconds: 5), () => (lirik));
}
Future<String> line2() async{
  String lirik = "Dan kita bertemu di belanga";
  return await Future.delayed(Duration(seconds: 3), () => (lirik));
}
Future<String> line3() async{
  String lirik = "Kisah yang ternyata tak seindah itu";
  return await Future.delayed(Duration(seconds: 4), () => (lirik));
}
Future<String> line4() async{
  String lirik = "Ku kira kita akan bersama";
  return await Future.delayed(Duration(seconds: 7), () => (lirik));
}
