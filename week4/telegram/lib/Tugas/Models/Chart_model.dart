import 'dart:ui';

class ChartModel {
  final String? name;
  final String? message;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.message, this.time, this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Jaehyun',
      message: 'Haii baby',
      time: '18.00',
      profileUrl:
          'https://0.soompi.io/wp-content/uploads/2017/11/20181806/Jaehyun.jpg'),
  ChartModel(
      name: 'renata',
      message: 'hello,how are you?',
      time: '9 march',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'Herjunot Ali',
      message: 'hello junot',
      time: '12.21',
      profileUrl:
          'https://static.republika.co.id/uploads/images/inpicture_slide/herjunot-ali-_180306141742-303.jpg'),
  ChartModel(
      name: 'Pevita',
      message: 'hello pev',
      time: '1 day',
      profileUrl:
          'https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2020/03/02/1490299168.jpg'),
  ChartModel(
      name: 'Enzy',
      message: 'hello Enzyy',
      time: '19.00',
      profileUrl:
          'https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2021/04/12/1286518702.jpg'),
  ChartModel(
      name: 'fadil',
      message: 'hello Fadil',
      time: '13.41',
      profileUrl:
          'https://akcdn.detik.net.id/community/media/visual/2021/05/17/fadil-jaidi_169.jpeg?w=700&q=90'),
  ChartModel(
      name: 'Bunga',
      message: 'Haii bunga',
      time: '12.21',
      profileUrl:
          'https://seremonia.id/wp-content/uploads/2019/03/IMG-20190322-WA0001.jpg'),
  
  ChartModel(
      name: 'Daniel',
      message: 'apa kabar niel',
      time: '25 january',
      profileUrl:
          'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1568632478/ltnhd6ucinariiwrllif.jpg'),
  
];